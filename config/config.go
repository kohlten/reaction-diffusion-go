package config

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	Da float64
	Db float64
	F  float64
	K  float64
	T  float64
	X  int
	Y  int
}

func (c *Config) Load() error {
	data, err := ioutil.ReadFile("config.json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, c)
	if err != nil {
		return err
	}
	return nil
}
