package main

import (
	"gitlab.com/kohlten/reactionDiffusion/config"
	"gitlab.com/kohlten/reactionDiffusion/game"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
)

func main() {
	cpu := startCpu()
	cfg := &config.Config{}
	err := cfg.Load()
	if err != nil {
		log.Fatalln("Failed to load config with error ", err)
	}
	g := game.NewGame(cfg)
	g.Run()
	//dumpMem()
	g.Close()
	stopCpu(cpu)
}

func startCpu() *os.File {
	fd, err := os.OpenFile("cpu.profile", os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0755)
	if err != nil {
		log.Fatalln(err)
	}
	err = pprof.StartCPUProfile(fd)
	if err != nil {
		log.Fatalln(err)
	}
	return fd
}

func stopCpu(fd *os.File) {
	pprof.StopCPUProfile()
	_ = fd.Close()
}

func dumpMem() {
	fd, err := os.OpenFile("mem.profile", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0755)
	if err != nil {
		log.Fatalln(err)
	}
	runtime.GC()
	err = pprof.WriteHeapProfile(fd)
	if err != nil {
		log.Fatalln(err)
	}
	_ = fd.Close()
}
