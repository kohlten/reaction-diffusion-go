#version 330 core

in vec2 TexCoords;

out vec4 color;

uniform sampler2D image;
uniform vec4 spriteColor;

void main()
{
    //vec4 c = /*spriteColor; */ texture(image, TexCoords);
    //if (c.r > 0 || c.g > 0 || c.b > 0)
    //    color = vec4(1.0, 0, 0, 1.0);
    //else
    //    color = vec4(0.0, 0.0, 0.0, 1.0);
    color = spriteColor * texture(image, TexCoords);
}