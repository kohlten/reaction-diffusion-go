package diffusion

import (
	"gitlab.com/kohlten/gamelib"
	"gitlab.com/kohlten/reactionDiffusion/config"
	"math"
	"time"
)

type Cell struct {
	a, b float64
}

type worker chan bool

type Diffusion struct {
	cfg           *config.Config
	running       bool
	paused        bool
	current, next [][]Cell
	workers       []worker
	size          int
	speed         int
	st            uint64
	stop          bool
	fps           *gamelib.FpsCounter
}

func NewDiffusion(cfg *config.Config) *Diffusion {
	d := new(Diffusion)
	d.cfg = cfg
	d.running = true
	d.size = d.cfg.X + 1
	d.init()
	d.initWorkers()
	d.fps = gamelib.NewFpsCounter()
	d.speed = 400
	go d.run()
	return d
}

func (d *Diffusion) Stop() {
	d.running = false
}

func (d *Diffusion) SetPaused(b bool) {
	d.paused = b
}

func (d *Diffusion) GetPaused() bool {
	return d.paused
}

func (d *Diffusion) GetSpeed() int {
	return d.speed
}

func (d *Diffusion) SetSpeed(speed int) {
	d.speed = speed
}

func (d *Diffusion) GetCurrent() [][]Cell {
	return d.current
}

func (d *Diffusion) GetFPS() uint32 {
	return d.fps.GetFps()
}

func (d *Diffusion) Reset() {
	d.stop = true
	d.paused = true
	time.Sleep(10 * time.Millisecond)
	d.current = nil
	d.next = nil
	d.init()
	d.paused = false
	d.stop = false
}

func (d *Diffusion) IsDone() bool {
	if d.current[1][1].b == 1.0 {
		return true
	}
	a := 0.0
	b := 0.0
	for i := 0; i < len(d.current); i++ {
		for j := 0; j < len(d.current[i]); j++ {
			a += d.current[i][j].a
			b += d.current[i][j].b
		}
	}
	return a < 0.000000001 || b < 0.000000001
}

func (d *Diffusion) AddA(i, j int) {
	d.current[i][j].a = 1.0
}

func (d *Diffusion) AddB(i, j int) {
	d.current[i][j].b = 1.0
}

func (d *Diffusion) run() {
	for d.running {
		d.st = gamelib.GetTimeNS()
		for d.paused {
			time.Sleep(1 * time.Millisecond)
		}
		doneWorkers := 0
		for _, w := range d.workers {
			w <- true
		}
		for doneWorkers < len(d.workers)-1 {
			for _, w := range d.workers {
				select {
				case <-w:
					doneWorkers += 1
				}
			}

		}
		d.swap()
		d.fps.Update()
		gamelib.LimitFPS(d.st, float64(d.speed))
	}
}

func (d *Diffusion) doOne(i, j int) {
	c := d.current[i][j]
	n := d.next[i][j]
	n.a = c.a + (d.cfg.Da * d.laplaceA(i, j)) - (c.a * c.b * c.b) + (d.cfg.F*(1-c.a))*d.cfg.T
	n.b = c.b + (d.cfg.Db * d.laplaceB(i, j)) + (c.a * c.b * c.b) - ((d.cfg.K+d.cfg.F)*c.b)*d.cfg.T
	n.a = math.Max(math.Min(n.a, 1.0), 0.0)
	n.b = math.Max(math.Min(n.b, 1.0), 0.0)
	d.current[i][j] = c
	d.next[i][j] = n
}

func (d *Diffusion) swap() {
	d.current, d.next = d.next, d.current
}

func (d *Diffusion) laplaceA(i, j int) float64 {
	sumA := d.current[i][j].a * -1
	sumA += d.current[i-1][j].a * 0.2
	sumA += d.current[i+1][j].a * 0.2
	sumA += d.current[i][j+1].a * 0.2
	sumA += d.current[i][j-1].a * 0.2
	sumA += d.current[i-1][j-1].a * 0.05
	sumA += d.current[i+1][j-1].a * 0.05
	sumA += d.current[i+1][j+1].a * 0.05
	sumA += d.current[i-1][j+1].a * 0.05
	return sumA
}

func (d *Diffusion) laplaceB(i, j int) float64 {
	sumB := d.current[i][j].b * -1
	sumB += d.current[i-1][j].b * 0.2
	sumB += d.current[i+1][j].b * 0.2
	sumB += d.current[i][j+1].b * 0.2
	sumB += d.current[i][j-1].b * 0.2
	sumB += d.current[i-1][j-1].b * 0.05
	sumB += d.current[i+1][j-1].b * 0.05
	sumB += d.current[i+1][j+1].b * 0.05
	sumB += d.current[i-1][j+1].b * 0.05
	return sumB
}

func (d *Diffusion) initWorkers() {
	workers := d.size / 3
	sizePerWorker := d.size / workers
	current := 1
	for i := 0; i < workers; i++ {
		w := worker(make(chan bool))
		d.workers = append(d.workers, w)
		end := current + sizePerWorker
		if i == workers-1 {
			end -= 1
		}
		go d.runWorker(w, current, end)
		current += sizePerWorker
	}
}

func (d *Diffusion) runWorker(w worker, start, end int) {
	for d.running {
		_ = <-w
		for i := start; i < end; i++ {
			for j := 1; j < d.size-1; j++ {
				if d.stop {
					goto exit
				}
				d.doOne(i, j)
			}
		}
	exit:
		w <- true
	}
}

func (d *Diffusion) init() {
	for i := 0; i < d.size; i++ {
		d.current = append(d.current, make([]Cell, d.size))
		for j := 0; j < d.size; j++ {
			d.current[i][j].a = 1.0
		}
	}
	d.next = d.current
	for i := (d.size / 2) - 10; i < (d.size/2)+10; i++ {
		for j := (d.size / 2) - 10; j < (d.size/2)+10; j++ {
			d.current[i][j].b = 1.0
		}
	}
}

func (c *Cell) GetA() float64 {
	return c.a
}

func (c *Cell) GetB() float64 {
	return c.b
}
