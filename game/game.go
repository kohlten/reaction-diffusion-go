package game

import (
	"fmt"
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/go-gl/mathgl/mgl32"
	"github.com/google/uuid"
	"github.com/inkyblackness/imgui-go"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/kohlten/gamelib"
	"gitlab.com/kohlten/glLib"
	"gitlab.com/kohlten/reactionDiffusion/config"
	"gitlab.com/kohlten/reactionDiffusion/game/diffusion"
	imgui_impl "gitlab.com/kohlten/reactionDiffusion/game/imgui-impl"
	"image"
	"image/color"
	"image/png"
	_ "image/png"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"
)

type ui struct {
	speed  int32
	da     string
	db     string
	f      string
	k      string
	t      string
	paused bool
}

var maxTime = uint64((10 * time.Minute).Milliseconds())

type Game struct {
	config   *config.Config
	window   *glLib.Window
	renderer *glLib.Renderer
	diff     *diffusion.Diffusion
	impl     *imgui_impl.ImguiImpl
	pixels   []byte
	ui       ui
	rightPressed bool
	leftPressed bool
	running  bool
	log *os.File
	st uint64
}

func NewGame(cfg *config.Config) *Game {
	g := new(Game)
	g.config = cfg
	g.createWindow()
	g.impl = imgui_impl.NewImgui(g.window.Window)
	g.createRenderer()
	g.running = true
	g.diff = diffusion.NewDiffusion(cfg)
	g.initUI()
	g.pixels = make([]byte, (g.config.X*g.config.X)*4)
	g.log, _ = os.OpenFile("log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0755)
	rand.Seed(int64(gamelib.GetTimeNS()))
	return g
}

func (g *Game) Run() {
	g.st = gamelib.GetTimeMS()
	for g.running {
		g.input()
		g.update()
		g.draw()
	}
}

func (g *Game) Close() {
	g.window.Free()
	_ = g.log.Close()
}

func (g *Game) input() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch eventType := event.(type) {
		case *sdl.QuitEvent:
			g.running = false
		case *sdl.MouseButtonEvent:
			if eventType.Type == sdl.MOUSEBUTTONDOWN {
				if eventType.Button == 1 {
					g.leftPressed = true
				} else if eventType.Button == 3 {
					g.rightPressed = true
				}
			} else {
				if eventType.Button == 1 {
					g.leftPressed = false
				} else if eventType.Button == 3 {
					g.rightPressed = false
				}
			}

		}
		g.impl.Platform.ProcessEvent(event)
	}
}

func (g *Game) update() {
	x, y, _ := sdl.GetMouseState()
	if g.rightPressed {
		g.diff.AddB(int(y), int(x))
	}
	if g.leftPressed {
		g.diff.AddA(int(y), int(x))
	}
	if g.diff.IsDone()|| gamelib.GetTimeMS() - g.st > maxTime  {
		g.diff.SetPaused(true)
		g.save()
		g.randomize()
		g.diff.Reset()
		g.diff.SetPaused(false)
		g.st = gamelib.GetTimeMS()
	}
}

func (g *Game) draw() {
	g.impl.Platform.NewFrame()
	imgui.NewFrame()
	g.impl.Renderer.PreRender([3]float32{})
	g.drawCells()
	g.drawUI()
	imgui.Render()
	g.impl.Renderer.Render(g.impl.Platform.DisplaySize(), g.impl.Platform.FramebufferSize(), imgui.RenderedDrawData())
	g.window.Window.GLSwap()
}

func (g *Game) drawCells() {
	cells := g.diff.GetCurrent()
	loc := 0
	for i := 0; i < g.config.X; i++ {
		for j := 0; j < g.config.X; j++ {
			a := cells[i][j].GetA()
			b := cells[i][j].GetB()
			au := byte(math.Max(math.Min(a*255, 255), 0))
			bu := byte(math.Max(math.Min(b*255, 255), 0))
			color := byte(math.Max(math.Min((a-b)*255, 255), 0))
			g.pixels[loc+0] = bu
			g.pixels[loc+1] = color
			g.pixels[loc+2] = color
			g.pixels[loc+3] = au
			loc += 4
		}
	}
	g.renderer.DrawPixels(g.pixels, gl.RGBA, gl.REPEAT, gl.REPEAT, gl.LINEAR, gl.LINEAR, mgl32.Vec2{-1, -1}, mgl32.Vec2{float32(g.config.X), float32(g.config.X)}, 0, mgl32.Vec4{1.0, 1.0, 1.0, 1.0})
}

func (g *Game) drawUI() {
	imgui.SetNextWindowSizeV(imgui.Vec2{X: 200, Y: 400}, imgui.ConditionAppearing)
	imgui.SetNextWindowPosV(imgui.Vec2{}, imgui.ConditionAlways, imgui.Vec2{})
	imgui.BeginV("Control Panel", nil, 0)
	if imgui.DragIntV("Speed", &g.ui.speed, 1.0, 0, 400, "%d") {
		g.diff.SetSpeed(int(g.ui.speed))
	}
	if imgui.Checkbox("Paused", &g.ui.paused) {
		g.diff.SetPaused(g.ui.paused)
	}
	imgui.InputTextV("da", &g.ui.da, imgui.InputTextFlagsCharsDecimal, nil)
	imgui.InputTextV("db", &g.ui.db, imgui.InputTextFlagsCharsDecimal, nil)
	imgui.InputTextV("f", &g.ui.f, imgui.InputTextFlagsCharsDecimal, nil)
	imgui.InputTextV("k", &g.ui.k, imgui.InputTextFlagsCharsDecimal, nil)
	imgui.InputTextV("t", &g.ui.t, imgui.InputTextFlagsCharsDecimal, nil)
	if imgui.Button("Reset") {
		g.convert()
		g.initUI()
	}
	if imgui.Button("Clear") {
		g.convert()
		g.diff.Reset()
		g.initUI()
	}
	if imgui.Button("To Default") {
		_ = g.config.Load()
		g.initUI()
	}
	imgui.Text("Diffuse FPS: " + strconv.FormatUint(uint64(g.diff.GetFPS()), 10))
	imgui.End()
}

func (g *Game) convert() bool {
	da, err := strconv.ParseFloat(g.ui.da, 64)
	if err != nil {
		fmt.Println("Failed to convert da with error ", err)
		return false
	}
	db, err := strconv.ParseFloat(g.ui.db, 64)
	if err != nil {
		fmt.Println("Failed to convert db with error ", err)
		return false
	}
	f, err := strconv.ParseFloat(g.ui.f, 64)
	if err != nil {
		fmt.Println("Failed to convert f with error ", err)
		return false
	}
	k, err := strconv.ParseFloat(g.ui.k, 64)
	if err != nil {
		fmt.Println("Failed to convert k with error ", err)
		return false
	}
	t, err := strconv.ParseFloat(g.ui.t, 64)
	if err != nil {
		fmt.Println("Failed to convert t with error ", err)
		return false
	}
	g.config.Da = da
	g.config.Db = db
	g.config.F = f
	g.config.K = k
	g.config.T = t
	return true
}

func (g *Game) save() {
	img := image.NewRGBA(image.Rect(0, 0, g.config.X, g.config.X))
	loc := 0
	for i := 0; i < g.config.X; i++ {
		for j := 0; j < g.config.X; j++ {
			img.SetRGBA(j, i, color.RGBA{R: g.pixels[loc], G: g.pixels[loc + 1], B: g.pixels[loc + 2], A: g.pixels[loc + 3]})
			loc += 4
		}
	}
	id := uuid.New().String()
	fd, err := os.OpenFile("screenshots/" + id + ".png", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0755)
	if err != nil {
		fmt.Println("Failed to open output " + id + ".png with error " + err.Error())
		return
	}
	err = png.Encode(fd, img)
	if err != nil {
		fmt.Println("Failed to encode image with error ", err)
		_ = fd.Close()
		return
	}
	_ = fd.Close()
	fmt.Println("Succesfully wrote to file " + id + ".png")
	g.convert()
	_, _ = g.log.Write([]byte(id + ".png: " + g.ui.da + ", " + g.ui.db + ", " + g.ui.f + ", " + g.ui.k + ", " + g.ui.t + "\n"))
}

func (g *Game) randomize() {
	g.config.Da = rand.Float64()
	g.config.Db = rand.Float64()
	g.config.F = rand.Float64()
	g.config.K = rand.Float64()
	g.config.T = rand.Float64()
	g.initUI()
}

func (g *Game) createWindow() {
	settings := glLib.NewWindowSettings()
	settings.Width = uint(g.config.X)
	settings.Height = uint(g.config.Y)
	settings.Name = "Reaction Diffusion"
	settings.Vsync = true
	settings.UseGl = true
	settings.Resizeable = false
	window, err := glLib.NewWindow(settings)
	if err != nil {
		log.Fatalln("Failed to create window with error ", err)
	}
	g.window = window
}

func (g *Game) createRenderer() {
	shader, err := glLib.NewShader("game/shaders/sprite.vert", "game/shaders/sprite.frag", "")
	if err != nil {
		log.Fatalln("Failed to open sprite shaders with error ", err)
	}
	gl.Enable(gl.BLEND)
	proj := mgl32.Ortho(0.0, float32(g.config.X), float32(g.config.Y), 0.0, -1.0, 1.0)
	shader.SetInt("image", 0, true)
	shader.SetMatrix4("projection", proj, false)
	g.renderer = glLib.NewRenderer(shader)
}

func (g *Game) initUI() {
	g.ui.speed = int32(g.diff.GetSpeed())
	g.ui.da = strconv.FormatFloat(g.config.Da, 'f', -1, 64)
	g.ui.db = strconv.FormatFloat(g.config.Db, 'f', -1, 64)
	g.ui.f = strconv.FormatFloat(g.config.F, 'f', -1, 64)
	g.ui.k = strconv.FormatFloat(g.config.K, 'f', -1, 64)
	g.ui.t = strconv.FormatFloat(g.config.T, 'f', -1, 64)
}
