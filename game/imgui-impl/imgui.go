package imgui_impl

import (
	"github.com/inkyblackness/imgui-go"
	"github.com/veandco/go-sdl2/sdl"
)

type ImguiImpl struct {
	Platform *SDL
	Renderer *OpenGL3
	Ctx      *imgui.Context
}

func NewImgui(window *sdl.Window) *ImguiImpl {
	im := new(ImguiImpl)
	im.Ctx = imgui.CreateContext(nil)
	io := imgui.CurrentIO()
	im.Platform = NewSDL(io, window)
	io.SetClipboard(im.Platform)
	im.Renderer = NewOpenGL3(io)
	return im
}